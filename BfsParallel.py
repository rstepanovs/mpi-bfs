import numpy as np
import itertools

import time
from mpi4py import MPI
import math

from Common import *

world = MPI.COMM_WORLD
world_size = world.size
rank = world.rank


def format_2d_position(flat, col_size):
    return "{},{}".format(flat // col_size, flat % col_size)


def normalize_array_index_for_process(vertice_absolute_index):
    return (vertice_absolute_index - rank) // world_size


def write_Result(path):
    output = "-1"
    if path:
        output = "{}\n{}".format(len(path), " ".join(list(map(lambda x: format_2d_position(x, m), path))))
    f = open('Output.txt', 'wt')
    f.write(output)


def bfs():
    time_for_calculation = 0.0
    global p, to_send
    if rank == 0:
        local_parents[0] = 0
    # fs_empty - no more vertices in local fs.
    # If all processes will have fs_empty, it will sum up to process count during reduction.
    # And exit condition will be met
    # If last node has been found, we can terminate all work.
    # In that case we assign it to world_size+1 to distinguish it from fs_empty
    last_node_found = 0
    # flag as a sum of fs_empty and last_node_found.
    # Only one flag to indicate both exit conditions to save allreduce call
    # How it works?
    # 1)If there are x processes and all's fs are empty, exit_condition will be x.
    # 2)If one process found finds last_node, exit_cindition will be x+1 thus also terminating work
    exit_condition = 0

    while exit_condition < world_size:
        received = []
        if fs:
            curr_root = fs.pop(0)
            ns = local_adjescencies[curr_root]
        else:
            curr_root = -1
            ns = []

        for p in range(0, world_size):
            serial_time = time.time()
            to_send = [(curr_root, v) for v in ns if v % world_size == p]
            time_for_calculation += time.time() -  serial_time
            world.send(to_send, p)
            received.extend(world.recv())
        # update parents
        serial_time = time.time()
        received = list(filter(lambda f: local_parents[normalize_array_index_for_process(f[1])] == -1, received))
        fs.extend([v[1] for v in received])
        for pair in received:
            i = normalize_array_index_for_process(pair[1])
            local_parents[i] = pair[0]
        if endpoint in fs:
            last_node_found = world_size + 1
        fs_empty = int(not fs)
        time_for_calculation += time.time() - serial_time
        exit_condition = world.allreduce(fs_empty + last_node_found, op=MPI.SUM)
    return time_for_calculation

if __name__ == '__main__':
    n, m, adjescencies = None, None, None
    # Vertice we need to find path to
    endpoint = -1
    local_adjescencies = None
    fs = []

    if rank == 0:
        n, m, adjescencies = translate_input()
        endpoint = n * m - 1
        fs.append(0)
        for p in range(0, world_size):
            to_send = {k: v for k, v in adjescencies.items() if k % world_size == p}
            if p == 0:
                local_adjescencies = to_send
            else:
                world.send(to_send, p)
    else:
        local_adjescencies = world.recv()

    start = time.time()
    endpoint = world.bcast(endpoint, root=0)
    v_count = endpoint + 1
    local_v_count = math.ceil(v_count / world_size)

    local_parents = [-1] * local_v_count
    time_for_calculation = bfs()
    time_for_calculation = world.reduce(time_for_calculation, op=MPI.MAX, root = 0)
    gathered = world.gather(local_parents, root=0)
    if rank == 0:
        result = [-1] * (endpoint + 1)
        for process_nr, parents in enumerate(gathered):
            for normal, parent in enumerate(parents):
                if parent == -1:
                    continue
                actual = normal * world_size + process_nr
                result[actual] = parent
        path = backtrack_parents(result, 0, endpoint)

        time_spent = time.time() - start
        print("Parallel algorithm executed in {} seconds with communication taking at most {}".format(time_spent, time_spent-time_for_calculation))
        write_Result(path)
    MPI.Finalize()
