from Common import translate_input
import time


def format_2d_position(flat, col_size):
    return "{},{}".format(flat // col_size, flat % col_size)


def backtrack_parents(result, start, end):
    path = [end]
    current = end
    while current != start:
        path.append(result[current])
        current = result[current]
        if current == -1:
            return None
    path.reverse()
    return path


def bfs():
    parents = [-1] * (n * m)
    queue = [0]
    parents[0] = 0

    while queue:
        s = queue.pop(0)
        for i in adjescencies[s]:
            if parents[i] == -1:
                queue.append(i)
                parents[i] = s
            if i == n * m - 1:
                return parents
    return parents


if __name__ == '__main__':

    n, m, adjescencies = translate_input()
    start = time.time()
    parents = bfs()

    path = backtrack_parents(parents, 0, n * m - 1)
    end = time.time()

    print("Serial algorithm executed in", time.time()-start, "seconds")
    output = "-1"
    if path:
        output = "{}\n{}".format(len(path), " ".join(list(map(lambda x: format_2d_position(x, m), path))))
    f = open('Output-Serial.txt', 'wt')
    f.write(output)
