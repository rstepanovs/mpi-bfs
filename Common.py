import numpy as np
import itertools

def translate_input():
    l, m, n = read_input()

    adjacencies = {}
    for rowIdx, row in enumerate(l):
        for colIdx, col in enumerate(row):
            if l[rowIdx, colIdx] == 1:
                continue
            rows = [rowIdx - 1, rowIdx + 1, rowIdx]
            cols = [colIdx - 1, colIdx + 1, colIdx]
            positions = list(map(lambda x: flat_1d_position(x[0], x[1], n, m, l), itertools.product(rows, cols)))
            current = rowIdx * m + colIdx
            positions = list(filter(lambda adj: adj != -1 and adj != current, positions))
            adjacencies[current] = positions
    return n, m, adjacencies


def read_input():
    with open("Input.txt") as input:
        n, m = map(int, input.readline().split())
        l = list(map(lambda row: row.split(), input.read().split("\n")))
    return np.array(list(list(map(int, x)) for x in l)), m, n

def backtrack_parents(result, start, end):
    path = [end]
    current = end
    while current != start:
        path.append(result[current])
        current = result[current]
        if current == -1:
            return None
    path.reverse()
    return path


def flat_1d_position(row, col, n, m, grid):
    if row < 0 or col < 0:
        return -1
    if row < n and col < grid[0].size and grid[row, col] == 0:
        return row * m + col
    return -1
