#1-d  parallel BFS

*[Source: Wikipedia][1]*



*Maze is being solved by translating it into graph with edges from empty cell to it's neighbour that is also a cell*
 
 
Because graph is sparse (at most 8 edges for each verice) - Algorithm parses input into graph represented by adjescency list. 
Parsing is done on initial process, but could be paralelised.
 
To ensure that the least idle processes at a time, algorith distributes adjescency lists by one in `index mod process_number` fashion. E.g, if there are 5 processes,
0-th process will receive adjescency lists for vertices 0, 5, 10, ...

##### BFS
Actual BFS routine is done by doing infinite loop in each process with **exit condition**, 
by exchanging edge between process a - b if edge starts in process a and ends in process b. 
This is done between each process pair even if there are no edges to send. This results in p<sup>2</sup> send/receive pairs. 
Also after each level allreduce is sent to evaluate if solution is reached.   

After while loop is exited, all local parent arrays are gathered on process 0 where path to end vertice is backtracked 


##### Room for improvement
* Parsing data into graph should be done in parallel
* Don't need to send/receive to all-to-all processes, only neighbor processes can be used.


##### Usage:

mpirun -n 4 python3 BfsParallel.py

[1]: https://en.wikipedia.org/wiki/Parallel_breadth-first_search

