import numpy as np


def generate_input(rows, cols):
    f = open('Input.txt', 'wt')
    matrix = np.random.randint(0, 2, size=(rows, cols))
    matrix[0][0] = 0
    matrix[rows-1][cols-1] = 0
    values = "\n".join(" ".join(str(x) for x in row) for row in matrix)

    f.write("{} {}\n{}".format(rows, cols, values))

if __name__ == '__main__':
    generate_input(1000, 2000)




